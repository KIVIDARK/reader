﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Reading
{
	public partial class Form1 : Form
	{
		String[] text;
		int widhtText = 40;
		int countStrings = 35;

		int maxWidhtText = 0;
		int maxHeightText = 0;

		DataTable numericTable = new DataTable("book");
		DataTable info = new DataTable("table");

		public static int page = 1;

		bool isLoadImage = false;

		Image image;

		Graphics graphics;

		Font font;

		public Form1()
		{
			InitializeComponent();
			this.MouseWheel += new MouseEventHandler(this_MouseWheel);
		}

		private void Form1_Shown(object sender, EventArgs e)
		{
			elemResize();
		}

		private void открытьToolStripMenuItem_Click(object sender, EventArgs e)
		{
			CloseFile();
			openFileDialog1.ShowDialog();
		}

		public int StringWidth(Object str)
		{
			return (int)graphics.MeasureString(str.ToString(), font).Width - (int)graphics.MeasureString("\0", font).Width;
		}

		public void FormatingBook()
		{
			String currentString = String.Empty;
			int counter = 0;
			int pos = 0;
			int lastWord = 0;
			int i = 0;
			numericTable.Rows.Add(new object[] { i, pos });
			while (i < text.Length)
			{
				while (counter < countStrings)
				{
					if (i < text.Length)
					{
						if (text[i].Length < widhtText)
						{
							pos = 0;
							counter++;
							i++;
						}
						else
						{
							int s = pos;
							String alfa = String.Empty;
							while (true)
							{
								if (s < widhtText)
								{
									if (text[i][s] != ' ')
									{
										alfa += text[i][s];
										s++;
									}
									else
									{
										alfa += ' ';
										currentString += alfa;
										alfa = String.Empty;
										s++;
									}
								}
								else
								{
									if (s >= text[i].Length)
									{
										pos = 0;
										currentString += alfa;
										alfa = String.Empty;
										currentString = String.Empty;
										counter++;
										i++;
										break;
									}
									if (text[i][s] != ' ')
									{
										if (maxWidhtText > StringWidth(currentString + alfa + text[i][s]))
										{
											alfa += text[i][s];
											s++;
										}
										else if (alfa.Length > widhtText)
										{
											pos = s;
											currentString += alfa;
											alfa = String.Empty;
											currentString = String.Empty;
											counter++;
											break;
										}
										else
										{
											pos = lastWord;
											alfa = String.Empty;
											currentString = String.Empty;
											counter++;
											break;
										}
									}
									else
									{
										alfa += ' ';
										lastWord = s;
										currentString += alfa;
										alfa = String.Empty;
										s++;
									}
								}
							}
						}
					}
					else
					{
						break;
					}
				}
				counter = 0;
				numericTable.Rows.Add(new object[] { i, pos });
			}
		}

		public void TextToDisplay(int curStr, int curPos)
		{
			String currentText = String.Empty;
			String currentString = String.Empty;
			int counter = 0;
			int pos = curPos;
			int lastWord = 0;
			for (int i = curStr; counter < countStrings; )
			{
				if (i < text.Length)
				{
					if (text[i].Length < widhtText)
					{
						pos = 0;
						currentText += text[i] + "\n";
						counter++;
						i++;
					}
					else
					{
						int s = pos;
						String alfa = String.Empty;
						while (true)
						{
							if (s < widhtText)
							{
								if (text[i][s] != ' ')
								{
									alfa += text[i][s];
									s++;
								}
								else
								{
									alfa += ' ';
									currentString += alfa;
									alfa = String.Empty;
									s++;
								}
							}
							else
							{
								if (s >= text[i].Length)
								{
									pos = 0;
									currentString += alfa;
									currentText += currentString + '\n';
									alfa = String.Empty;
									currentString = String.Empty;
									counter++;
									i++;
									break;
								}
								if (text[i][s] != ' ')
								{
									if (maxWidhtText > StringWidth(currentString + alfa + text[i][s]))
									{
										alfa += text[i][s];
										s++;
									}
									else if (alfa.Length > widhtText)
									{
										pos = s;
										currentString += alfa;
										currentText += currentString + '\n';
										alfa = String.Empty;
										currentString = String.Empty;
										counter++;
										break;
									}
									else
									{
										pos = lastWord;
										currentText += currentString + '\n';
										alfa = String.Empty;
										currentString = String.Empty;
										counter++;
										break;
									}
								}
								else
								{
									alfa += ' ';
									lastWord = s;
									currentString += alfa;
									alfa = String.Empty;
									s++;
								}
							}
						}
					}
				}
				else
				{
					break;
				}
			}
			pictureBox1.Image = CreateBitmapImage(currentText);
			image = pictureBox1.Image;
			isLoadImage = true;
			elemResize();
		}

		private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
		{
			numericTable.Rows.Clear();
			info.Rows.Clear();
			page = 1;
			text = System.IO.File.ReadAllLines(openFileDialog1.FileName, Encoding.Default);
			TextToDisplay(0, 0);
			String fileName = "info-" + Path.GetFileName(openFileDialog1.FileName) + ".inf";
			String dataFile = "num-" + Path.GetFileName(openFileDialog1.FileName) + ".inf";
			if (File.Exists(fileName))
			{
				info.ReadXml(fileName);
				if (info.Rows[0]["Date"].ToString()
					== new FileInfo(openFileDialog1.FileName).LastWriteTime.ToString())
				{
					numericTable.ReadXml(dataFile);
					page = int.Parse(info.Rows[0]["Num"].ToString()) + 1;
				}
				else
				{
					FormatingBook();
				}
			}
			else
			{
				info.Rows.Add(0, 0);
				FormatingBook();
			}
			ReloadPage();
		}

		private Bitmap CreateBitmapImage(string imageText)
		{

			Bitmap bitmap = new Bitmap(1, 1);

			bitmap = new Bitmap(bitmap, new Size(maxWidhtText, maxHeightText));

			graphics = Graphics.FromImage(bitmap);


			graphics.Clear(Color.White);

			graphics.SmoothingMode = SmoothingMode.HighSpeed;
			graphics.TextRenderingHint = TextRenderingHint.SystemDefault;

			graphics.DrawString(imageText, font, new SolidBrush(Color.FromArgb(20, 20, 20)), 0, 0);
			graphics.Flush();
			return (bitmap);
		}

		public void elemResize()
		{
			if (isLoadImage)
			{
				pictureBox1.Width = int.Parse(Math.Round(pictureBox1.Height / 1.2).ToString());
				if (pictureBox1.Width > this.Width + 10)
				{
					this.Width = pictureBox1.Width + 10;
				}
				pictureBox1.Image = (Image)image.Clone();
				Graphics graphics = Graphics.FromImage(pictureBox1.Image);
				Rectangle sorceRectangle = new Rectangle(0, 0, image.Width, image.Height);
				Rectangle destRectangle = new Rectangle(0, 0, pictureBox1.Width, pictureBox1.Height);
				graphics.DrawImage(pictureBox1.Image, destRectangle, sorceRectangle, GraphicsUnit.Pixel);
			}
		}

		private void Form1_SizeChanged(object sender, EventArgs e)
		{
			elemResize();
		}

		public void ChangeFont()
		{
			Font font = new Font("Arial", 24, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);

			String a = String.Empty;
			String b = String.Empty;
			for (int i = 0; i < widhtText; i++)
			{
				a += "W";
			}
			for (int i = 0; i < countStrings; i++)
			{
				b += "\n";
			}
			maxWidhtText = (int)graphics.MeasureString(a, font).Width;
			maxHeightText = (int)graphics.MeasureString(b, font).Height;
		}

		private void Form1_Load(object sender, EventArgs e)
		{
			numericTable.Columns.Add("StrPos");
			numericTable.Columns.Add("AlfaPos");
			info.Columns.Add("Num");
			info.Columns.Add("Date");
			Bitmap bitmap = new Bitmap(1, 1);
			graphics = Graphics.FromImage(bitmap);
			font = new Font("Arial", 24, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
			ChangeFont();
		}

		private void выходToolStripMenuItem_Click(object sender, EventArgs e)
		{
			Application.Exit();
		}

		public void ReloadPage()
		{
			TextToDisplay(
				int.Parse(numericTable.Rows[page - 1]["StrPos"].ToString()),
				int.Parse(numericTable.Rows[page - 1]["AlfaPos"].ToString())
			);
			this.Name = Path.GetFileName(openFileDialog1.FileName) + " : " + page.ToString();
			toolStripStatusLabel1.Text = "Page : " + page.ToString();
		}

		private void Form1_KeyUp(object sender, KeyEventArgs e)
		{
			switch (e.KeyCode)
			{
				case Keys.Right:
					if (page < numericTable.Rows.Count)
					{
						page++;
						ReloadPage();
					}
					break;
				case Keys.Left:
					if (page > 1)
					{
						page--;
						ReloadPage();
					}
					break;
				default:
					break;
			}
		}

		private void this_MouseWheel(object sender, MouseEventArgs e)
		{
			if (e.Delta > 0 && page < numericTable.Rows.Count)
			{
				page++;
				ReloadPage();
			}
			if (e.Delta < 0 && page > 1)
			{
				page--;
				ReloadPage();
			}
		}

		private void CloseFile()
		{
			if (isLoadImage)
			{
				FileInfo file = new FileInfo(openFileDialog1.FileName);
				info.Rows[0]["Num"] = page - 1;
				info.Rows[0]["Date"] = file.LastWriteTime;
				info.WriteXml("info-" + Path.GetFileName(openFileDialog1.FileName) + ".inf");
				numericTable.WriteXml("num-" + Path.GetFileName(openFileDialog1.FileName) + ".inf");
			}
		}

		private void Form1_FormClosing(object sender, FormClosingEventArgs e)
		{
			CloseFile();
		}

		private void toolStripMenuItem1_Click(object sender, EventArgs e)
		{
			GoToPage gtp = new GoToPage(numericTable.Rows.Count);
			gtp.ShowDialog();
			if (gtp.DialogResult == System.Windows.Forms.DialogResult.OK)
			{
				ReloadPage();
			}
		}
	}
}
