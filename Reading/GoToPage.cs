﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Reading
{
	public partial class GoToPage : Form
	{
		int countPage = 0;

		public GoToPage(int countPage)
		{
			InitializeComponent();
			this.countPage = countPage;
		}

		private void domainUpDown1_SelectedItemChanged(object sender, EventArgs e)
		{
			
		}

		private void GoToPage_Shown(object sender, EventArgs e)
		{
			for (int i = countPage + 1; i > 0; i--)
			{
				domainUpDown1.Items.Add(i);
			}
			domainUpDown1.SelectedItem = 1;
		}

		private void button1_Click(object sender, EventArgs e)
		{
			Form1.page = int.Parse(domainUpDown1.SelectedItem.ToString());
			DialogResult = System.Windows.Forms.DialogResult.OK;
		}
	}
}
